The JPL site is arranged in a hierarchial directory structure. At the top level
is the owner (JPL), at the next level is the instrument (beckman). At the next level
is the type (minerals) and the next level is the class (e.g. arsenate). In each class
directory there are 2 subdirectories (anc and txt). The anc directory includes all the 
ancillary information files and the txt directory includes all the data files for the class.
The only difference between the data filename and the ancillary filename is the last 
character before the .txt extension is replaced with an "a" to denote ancillary. Note the 
last character before the .txt extension in the data files refers to the particle size 
(C,M and F for coarse, medium and fine respectively).
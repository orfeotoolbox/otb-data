Mineral name:  Antigorite (Mg, Fe+2)3 Si2O5(OH)4

Sample No.: antigor.1

Locality:  Kalsertal, Tyrol, Austria.

Source:  NMNH B17958 (sample borrowed from Jim Crowley, USGS).

Petrographic Description:  The sample was a large green single 
crystal.  It appeared to be mostly pure antigorite under the 
microscope, but some (<3%) alteration or impurity was present, 
possibly chrysotile.

XRD Analysis:  Pure magnesian antigorite (analysis by Jim Crowley).

Chemistry:  Not available.

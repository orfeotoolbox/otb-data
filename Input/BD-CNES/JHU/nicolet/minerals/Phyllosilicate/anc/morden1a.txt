Mineral:  Mordenite (Ca,Na2,K2)Al2Si10O24.7H2O

Sample No.: morden.1

XRD Analysis:  Pure mordenite.

Chemistry:  Volatile analysis by C. S. Papp, J. D. Sharkey, and K. J. 
Curry, and XRF analysis of the non-volatiles by J. Taggare, A. Bartel, 
and D. Diems, USGS Branch of Analytical Chemistry, Denver, showed:

SiO2	67.30
Al2O3	11.70
Fe2O3	0.88
MgO	0.35
CaO	2.72
K2O	3.54
Na2O 	1.08
TiO2	0.09
P2O5	<0.05
MnO 	0.10
H2O	11.10
Other LOI	0.80
	_____
Total 	99.71

Mineral:  Muscovite KAl2(Si3Al)O10(OH,F)2

Sample No.: muscov.1

XRD Analysis:  Pure muscovite.  

Chemistry:  Chemical analysis by R. A. Robie and B. S. Hemingway, 
USGS, Reston, showed that part of the same sample had the 
composition:

SiO2	46.29
Al2O3	35.82
FeO	2.27
Mg0	0.65
Na2O	0.45
K2O	10.25
H2O+	4.27
H2O- 	4.27
	_____
Total	*100.00
	*Excludes H2O-.

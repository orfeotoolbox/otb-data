Mineral:  Jadeite Na(Al,Fe+3)Si2O6

File Name:  jadeit1a.txt

XRD Analysis:  Jadeite with a trace of another unidentified phase 
having diffraction peaks at 3.42, 2.233, and 1.522 angstroms.  (This 
other phase probably accounts for the strong hydroxyl bands in the 
spectrum which are located near 2.8 microns.  The rest of the 
spectrum appears due to jadeite alone.)

Chemistry:  Microprobe analysis showed that this sample departs 
from the ideal composition.  It is somewhat heterogeneous with 
respect to CaO, which varies from 0.3% to 1.7% by weight as Na2O 
varies from 17.5% to 15.8%.  Average of 10 analyses:

SiO2	60.21
Al2O3	25.41
FeO	0.27
MgO	0.44
CaO	0.64
K2O	0.01
Na2O	14.06
TiO2	0.04
Cr2O3	0.01
	_____
Total 	101.09

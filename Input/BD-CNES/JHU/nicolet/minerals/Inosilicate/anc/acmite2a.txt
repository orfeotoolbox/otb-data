Sample No.: Acmite.2

XRD Analysis:  Relatively large (75-250 �m) grains were X-rayed.  
The resulting pattern showed acmite peaks and an unidentified peak 
of trace proportions at 4.48 angstroms.

Chemistry:  Microprobe analysis showed that one grain out of four 
examined had about 2 wt % more CaO and correspondingly less Na2O.  
Otherwise the sample appeared homogeneous, especially within 
grains.  An average of 13 analyses, which indicates close to end 
member composition, is: 

SiO2 	 52.08
Al2O3	0.98
FeO 	 29.22
MgO	0.06
CaO 	3.84
K2O	0.02
Na2O	 11.87
TiO2	 0.56
MnO 	0.24
	_____
Total 	98.87

Mineral:  Natrolite Na2Al2Si3O10.2H2O

Sample No.: natrol.1

XRD Analysis:  Pure natrolite.

Chemistry:  Volatile analysis by C. S. Papp, J. D. Sharkey, and K. J. 
Curry, and XRF analysis of the non-volatiles by J. Taggare, A. Bartel, 
and D. Diems, USGS Branch of Analytical Chemistry, Denver, showed:

SiO2	47.40
Al2O3 	26.60
FeTO3	0.08
MgO	0.17
CaO 	<0.02
K2O	<0.02
Na2O	15.60
TiO2	<0.02
MnO	<0.02
P2O5 	<0.05
H2O	9.21
Other LOI	0.45
	_____
Total	99.63

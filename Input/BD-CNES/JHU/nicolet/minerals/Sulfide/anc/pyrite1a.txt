Mineral:  Pyrite FeS2

Sample No.:  pyrite.1

XRD Analysis:  Pyrite and a small amount of some unidentified 
contaminant with a diffraction peak at 9.1 angstoms.  

Chemistry:  Microprobe analysis of the Smithsonian sample was not 
reliable because of calibration problems, but did show the presence 
of minor silica.  The complex  spectral features in the spectrum of the 
finer particle size range between 8 and 14 micrometers looked 
anomalous and possibly due to an unknown silicate, so additional 
samples of fine pyrite were measured.  These high-purity samples, 
donated by Amy Wayne of Pennsylvania State University, were 
analyzed at that institution by X-ray diffraction and spectrochemical 
cation analysis.  One sample, from Italy (#ITA), showed less than 
0.1% abundance of all cations other than Fe, except for 0.5% Si.  This 
sample displayed a spectrum almost identical to that shown for the 
fine fraction of our sample, including the anomalous fine structure 
and a similar unidentified X-ray peak.  Thus, these spectral features 
may, indeed, be due to a silicate fraction, but may also be common in 
pyrite.  The other sample, from Peru (#PER), showed less than 0.1% 
abundance of all cations other than Fe, except for 0.11% As.  This 
sample did not display the anomalous fine structure, but did show 
the stronger overtone and combination bands between the Fe-S 
fundamental stretching vibration band near 24 micrometers and the 
ubiquitous water band near 3 micrometers.  

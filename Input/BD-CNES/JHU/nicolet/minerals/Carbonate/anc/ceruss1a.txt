Mineral:  Cerussite, PbCO3

Sample No.: ceruss.1

XRD Analysis:  Pure cerrussite.

Chemistry:  Volatile analysis by C. S. Papp, J. D. Sharkey, and K. J. 
Curry and XRF analysis of the non-volatiles by J. Taggare, A. Bartel, , 
and ICP cation analysis by P. H. Briggs, USGS Branch of Analytical 
Chemistry, Denver, showed a low total but expected oxide ratios of 
Pb and C: 

PbO	82.62
CO2	15.10
H2O	<0.05
	_____
Total	97.77

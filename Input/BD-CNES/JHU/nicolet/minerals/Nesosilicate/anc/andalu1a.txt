Mineral:  Andalusite Al2SiO5

Sample No.: andalus.1

XRD Analysis:  Pure andalusite.

Chemistry:  Microprobe analysis showed the sample to be 
homogeneous within and between grains.  An average of 10 analyses, 
indicating close to end member composition, is:

SiO2 	37.19
Al2O3	63.62
FeO 	0.26
MgO	0.06
CaO 	0.02
K2O	0.01
Na2O	0.02
TiO2	0.03
MnO 	0.01
	_____
Total 	101.22

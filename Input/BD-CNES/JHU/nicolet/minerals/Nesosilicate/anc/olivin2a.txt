Mineral:  Olivine (Fo18) (Fe+2,Mg)2SiO4

Sample No.:  olivine.2

XRD Analysis:  None.

Chemistry:  Microprobe analysis by W.I. Ridley, USGS showed good 
Fo18 composition:

SiO2	31.11
TiO2	0.06
Al2O3	0.00
Cr2O3	0.07
MnO	1.30
NiO	0.12
FeO 	59.75
MgO 	7.71
CaO	0.09
	_____
Total	100.21

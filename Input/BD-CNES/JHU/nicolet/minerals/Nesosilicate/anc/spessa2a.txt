Mineral:  Spessartine Mn3Al2 (SiO4)3

Sample No.:  spessart.2

XRD Analysis:  Pure spessartine.

Chemistry:  Microprobe analysis found one heterogeneous area 
where all oxides, including silica, summed to only 27 wt %.  
Otherwise, FeO varies from 7.5 to 12%, with inverse variation in MnO.  
Average of 10 analyses (other than anomalous point):

SiO2	35.02
Al2O3	20.74
FeO	10.40
MgO	0.04
CaO	1.61
K2O	0.03
Na2O	0.09
TiO2	0.07
MnO	30.82
Total	98.81

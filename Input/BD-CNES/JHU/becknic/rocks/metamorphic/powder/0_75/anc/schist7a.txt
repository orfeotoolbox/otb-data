Muscovite subhedra occur throughout the section and there was scattered 
sphene as well.  Modes were 71.6% actinolite-tremolite, 17.6% chlorite, 7.2% 
garnet, 2.8% muscovite and 0.8% sphene.

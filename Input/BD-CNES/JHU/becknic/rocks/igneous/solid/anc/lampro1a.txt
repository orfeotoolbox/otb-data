Composition:

Petrographic Description:  A partially altered, inequigranular rock with a
felty fabric consisting of 58% sanidine, 28% pale green pyroxene,7.6% 
opaque and 6.3% olivine anhedra.  Large pyroxene grains are occasionally 
twinned and most basal sections show oscillatory zoning.  The olivine grains 
are generally much smaller than the large pyroxenes and show iddingsite 
alteration along fractures.  Many olivines are completely altered to a 
yellowish- or reddish- brown mineral (iddingsite?).  The sanidine laths show 
Carlsbad twinning, and often oscillatory zoning as well.  Small opaque 
anhedra are scattered throughout the sample.  In addition a yellowish brown 
alteration product (limonite?) occurs throughout the section.

Microprobe Analysis:  The pyroxene phase proved to be ferrian augite.
Compositionally the olivine was Fo55 while opaques probed were all
titanomagnetite.  Some sericite was indicated in the analysis.

Chemical Analysis:
SiO2  45.94
CaO  12.06
TiO2  1.57
Na2O  3.04
Al2O3  16.09
K2O  1.49
Fe2O3  4.21
H2O  1.47
FeO  7.34
P2O5  0.62
MnO  0.19
MgO  6.53
TOTAL  100.55

Composition:

Petrographic Description:  A mosaically-textured aggregate of anhedral 
olivine with serpentine alteration along some of the fractures.  Chromite (?) 
occurs as scattered subhedra.  The modal analysis gave: 89% olivine, 8% 
serpentine, and 3% opaque.

Microprobe Analysis:  Microprobe analysis revealed an olivine composition 
of F091.  The opaque phase showed Fe >AL >Mg with little titanium, and is
probably chromite.  Serpentine (antigorite) had 4% substitution of FeO for
MgO.  Minor chlorite approximated clinochlore in composition.

Chemical Analysis:
SiO2  39.57
CaO  0.05
TiO2  0.004
Na2O  0.0
Al2O3  0.44
K2O  0.004
Fe2O3  1.62
H2O  2.85
FeO  7.47
P2O5  0.0
MnO  0.16
MgO   47.6
TOTAL  99.76

Very friable and slightly effervescent, non sticky and non plastic, moderately 
alkaline (pH 8.2).  0.13% organic carbon; 4.0% clay; 8.3% silt; 87.7% sand.  
Trace of CaCO3.

Clay mineralogy: none.  

Coarse mineralogy (B horizon at 35 cm): 62% quartz, 16% potassic feldspar, 
10% other, 4% opaque, 3% biotite, 2% plagioclase feldspar, 2% hornblende, 
1% zircon, traces of calcite and glass.

Rock Type: 

Mineral: Calcite

Sample No.: ro337

XRD Analysis:  Calcite 99%

Chemistry:

SiO2      0.23
Al2O3     0.14
Fe2O3    <0.10
CaO      54.78
TiO2     <0.01
MnO      <0.01
K2O      <0.10
MgO       0.87
Na2O      0.13
P2O5     <0.01
CO2      44.02
Total    99.90

              
            
 


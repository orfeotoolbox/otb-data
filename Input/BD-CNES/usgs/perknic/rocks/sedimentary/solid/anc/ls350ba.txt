Rock Type:

Mineral: Calcite

Sample No.: ro350b

XRD Analysis:  Calcite 98%, Quartz <1%, Chlorite <1%

Chemistry:

SiO2      0.56
Al2O3     0.26
Fe2O3    <0.10
CaO      54.38
TiO2     <0.01
MnO      <0.01
K2O      <0.10
MgO       0.98
Na2O      0.05
P2O5     <0.01
         43.70
Total    99.93

              
            
 


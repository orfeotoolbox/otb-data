Lithology      Rhyolite
Sample#        AP-936-10
Chemistry
SiO2           79.85
Al2O3          11.20
Fe             0.95
CaO            0.20
TiO2           0.11
MnO            0.01
K2O            4.28
MgO            0.23
Na2O           2.97
P2O5           0.02
PPC            0.18
